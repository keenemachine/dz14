package test;

import test.Pages.PageObjectSupplier;
import org.testng.annotations.Test;

public class RezonitNewTest implements PageObjectSupplier {

    @Test
    public void newTestPositive() {
        rezonitMainPage().openMainPage();
        rezonitMainPage().openPcbFastConstructor();
        rezonitConstructorPage().setParametersToConstructor("10", "20", "20");
        rezonitConstructorPage().clickCalculateBtn();
        rezonitConstructorPage().checkPriceDisplayed(true);
    }

    @Test
    public void newTestNegative() {
        rezonitMainPage().openMainPage();
        rezonitMainPage().openPcbFastConstructor();
        rezonitConstructorPage().setParametersToConstructor("10", "-999", "");
        rezonitConstructorPage().checkAlertDisplayed(true);
    }
}
