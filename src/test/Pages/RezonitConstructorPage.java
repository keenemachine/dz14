package test.Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class RezonitConstructorPage {

    SelenideElement inputLength = $x("//input[@id='calculator-input-1']");
    SelenideElement inputWidth = $x("//input[@id='calculator-input-2']");
    SelenideElement inputNumber = $x("//input[@id='calculator-input-3']");
    SelenideElement calculateButton = $x("//button[@id='calculate']");
    SelenideElement price = $x("//span[@id='total-price']");
    SelenideElement alert = $x("//div[@class='alert alert-danger']");

    public void setParametersToConstructor(String length, String width, String number) {
        inputLength.sendKeys(length);
        inputWidth.sendKeys(width);
        inputNumber.sendKeys(number);
    }

    public void clickCalculateBtn() {
        calculateButton.click();
    }

    public void checkPriceDisplayed(boolean displayed) {
        sleep(10000);
        if (displayed) {
            price.should(exist).shouldBe(visible);
        } else {
            price.should(disappear).shouldBe(hidden);
        }
    }

    public void checkAlertDisplayed(boolean displayed) {
        if (displayed) {
            alert.shouldBe(visible).shouldHave(text("Ширина болжна быть от 5 до 475 мм"));
        } else {
            alert.shouldBe(hidden);
        }
    }
}
