package test.Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class BookPage {

    SelenideElement ppdPrice = $x("//meta[@itemprop='price']");
    SelenideElement abonementPrice = $x("//div[@data-test-id='book-sale-block__abonement--wrapper']");
    SelenideElement addToCartBtn = $x("//button[@data-test-id='book__addToCartButton--desktop']");

    public void checkPpdPriceDisplayed(boolean displayed) {
        if (displayed) {
            ppdPrice.shouldBe(exist);
        } else {
            ppdPrice.shouldBe(disappear);
        }
    }

    public void checkAbonementPriceDisplayed(boolean displayed) {
        if (displayed) {
            abonementPrice.shouldBe(visible);
        } else {
            abonementPrice.shouldBe(hidden);
        }
    }

    public void checkAddToCartDisplayed(boolean displayed) {
        if (displayed) {
            addToCartBtn.shouldBe(visible);
        } else {
            addToCartBtn.shouldBe(hidden);
        }
    }
}
