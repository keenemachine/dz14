package test.Pages;

public interface PageObjectSupplier {

    default LitresMainPage litresMainPage() {
        return new LitresMainPage();
    }

    default BookPage bookPage() {
        return new BookPage();
    }

    default RezonitMainPage rezonitMainPage() {
        return new RezonitMainPage();
    }

    default RezonitConstructorPage rezonitConstructorPage() {
        return new RezonitConstructorPage();
    }
}
