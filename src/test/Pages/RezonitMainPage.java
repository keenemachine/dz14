package test.Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class RezonitMainPage {

    SelenideElement pcbMenu = $x("//header/div[2]/div/div/ul/li[1]");

    public void openMainPage() {
        open("https://www.rezonit.ru/");
        sleep(10000);
    }

    public void openPcbFastConstructor() {
        pcbMenu.click();
    }
}
