package test.Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class LitresMainPage {

    SelenideElement inputString = $x("//input[@data-test-id='header__search-input--desktop']");
    SelenideElement findBtn = $x("//button[@data-test-id='header__search-button--desktop']");
    SelenideElement gridElement = $x("//div[@data-test-id='art__cover--desktop']");

    public void openMainPage() {
        open("https://www.litres.ru/");
    }

    public void searchBySearchString(String title) {
        inputString.sendKeys(title);
        findBtn.click();
    }

    public void selectFirstBook() {
        gridElement.click();
    }
}
