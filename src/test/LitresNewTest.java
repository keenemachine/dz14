package test;

import test.Pages.PageObjectSupplier;
import org.testng.annotations.Test;

public class LitresNewTest implements PageObjectSupplier {

    @Test
    public void newTest() {
        litresMainPage().openMainPage();
        litresMainPage().searchBySearchString("грокаем алгоритмы");
        litresMainPage().selectFirstBook();
        bookPage().checkAbonementPriceDisplayed(false);
        bookPage().checkPpdPriceDisplayed(true);
        bookPage().checkAddToCartDisplayed(true);
    }
}
